#include <iostream>
#include <fstream>
//#include "../Gauss/Sim/LbDelphes/src/SimpleClass/SimpleClass.cpp"
#include "../Gauss/Sim/LbDelphes/src/SimpleClass/ECALclasses.h"
using namespace std;
#include <sys/types.h>
#include <sys/stat.h>
#include "TChain.h"
#include "../tools/Tools.h"
struct stat info;


int myTest(){
	const Int_t Njobs = 1; 
	cout << "********************************\n";
  	cout << "*           LOADING            *\n";
  	cout << "********************************\n";
	ECal_Geo ECALgeo;
	std::ifstream is_geo("./ECalGeo.dat", std::ios::in | std::ios::binary);
	ECALgeo.read(is_geo);
	TChain tin_gen("genTree");
	for (Int_t i=0; i<Njobs;++i){
	  TString finCells = "";
	  finCells.Form(paths::path_EOS_minBias_upgrade0+"%d/genTuple.root*", i);
	  //if (stat( finCells.Data(), &info ) != 0) {cout << "file: " << finCells.Data() << " not found!!\n"; continue;}
	  Int_t Nfile_added = tin_gen.Add(finCells);
	  if (Nfile_added==0)
	    {cout << "file: " << finCells.Data() << " not found!!\n"; continue;}
	}
	cout << "total number of entries: " << tin_gen.GetEntries() << endl; 
	ECALevents myEvents;
	cout << "ECALevents::read ... \n";
	myEvents.read(&tin_gen,&ECALgeo);
	cout << "\nNumber of events read: " << myEvents.m_Container.size() << endl;
	myEvents.getOccupancy_cell("CellOccupancy");
	cout << "Clustering .....\n";	
	myEvents.buildClusters(5);
	cout << "MonitoringGEN ..\n";
	myEvents.clusterMonitor("clusterGEN",20);


	cout << "********************************\n";
  	cout << "*            REC               *\n";
  	cout << "********************************\n";
	cout << "Smearing .......\n";
	myEvents.reco(0.1, 0.01, 2., 0);
	cout << "Seed Search ....\n";
	myEvents.findSeeds(0, 50);
	cout << "Clustering .....\n";
	myEvents.buildClusters(5);
	cout << "MonitoringREC ..\n";
	myEvents.clusterMonitor("clusterREC",20);
	cout << "Storaging ......\n";
	myEvents.saveTreeParticles("./recTuple", "REC", true);
	
	
	cout << "********************************\n";
  	cout << "*         RESOLUTION           *\n";
  	cout << "********************************\n";
	TFile fin_rec("./recTuple.root", "READ");
	TTree *tin_rec = (TTree*)fin_rec.Get("REC");
	cout << "==> Init. ECALres\n";
	ECALres res(&tin_gen, tin_rec, &ECALgeo);
	cout << "==> Matching...\n";
	res.matching(0.25,4);
	cout << "==> Filling Res. Tree\n";
	res.fillTreeRes();
	cout << "==> Saving Res. Tree\n";
	res.saveTreeRes("resTuple");
	fin_rec.Close();
	vector<Int_t>    statsMatchYields = res.getStats_MatchYields(); 
	vector<Double_t> statsMatchProbs  = res.getStats_MatchProbs(); 
	cout << "\nN matches      :";
	for (auto v : statsMatchYields) cout << "    " << v; 
	cout << "\nProbabilities  :";
	for (auto v : statsMatchProbs)  cout << "    " << v; 
	cout << endl;

	TFile fout_gen("genTuple.root", "RECREATE");
	tin_gen.CloneTree(-1);
	fout_gen.Write();
	fout_gen.Close();


	cout << "********************************\n";
  	cout << "*         OCCUPANCY            *\n";
  	cout << "********************************\n";
	std::vector<Double_t> v_occupancy = myEvents.getOccupancy_mean(0);
	std::cout << " ==> Total occup. : " << v_occupancy[0]*100 << " %\n";
	std::cout << " ==> Reg.1 occup. : " << v_occupancy[1]*100 << " %\n";
	std::cout << " ==> Reg.2 occup. : " << v_occupancy[2]*100 << " %\n";
	std::cout << " ==> Reg.3 occup. : " << v_occupancy[3]*100 << " %\n";
	
	
	return 1;
}

