import os

tag1         = 'fullSim'
tag2         = 'eventType_30000000'
date         = '20190613'
eventType   = 30000000
Nevt_perjob = 5
Nsubjobs    = 1
runNumber   = 1082
firstEvtNumber = 1
nTupleFileOut = 'TEST.root'
recreate = True
#############################################################
path_output = '/storage/gpfs_data/local/lhcb/users/dmanuzzi/ECAL/{tag1}/{tag2}/'.format(tag1=tag1, tag2=tag2)
path_gauss_run = '/home/LHCB/dmanuzzi/ECAL/Gauss/build.x86_64-centos7-gcc7-opt/'
path_config = '/home/LHCB/dmanuzzi/ECAL/fullSim/sim/'
if not os.path.exists(path_output): os.makedirs(path_output)
else:
    if recreate and ('/storage/gpfs_data/local/lhcb/users/dmanuzzi/ECAL/{tag1}/{tag2}'.format(tag1=tag1, tag2=tag2) in path_output):
        os.system('rm -r %s/*'%(path_output))
#if not os.path.exists(path_config): os.mkdir(path_config)
#else:  
#    if 'config' in path_config: 
#        os.system('rm -r %s/*'%(path_config))


for i in range(0,Nsubjobs):
    print i
    path_subjob_out = path_output+'%d/'%(i)
    #path_subjob_conf= path_config+'%d/'%(i)
    os.mkdir(path_subjob_out)
    #os.mkdir(path_subjob_conf)

    f_gauss_job_template = open(path_config+'Gauss-Job_parallel.txt', 'r')
    s_gauss_job_template = f_gauss_job_template.read()
    f_gauss_job_template.close()
    s_gauss_job = s_gauss_job_template.format( eventType=eventType,
                                               Nevt=Nevt_perjob,
                                               runNumber=runNumber,
                                               FirstEvtNumber=int(firstEvtNumber+i*Nevt_perjob)
                                               )
    f_gauss_job = open(path_subjob_out+'Gauss-Job_parallel.py', 'w')
    f_gauss_job.write(s_gauss_job)
    f_gauss_job.close()

    f_bender_job_template = open(path_config+'benderMacro.txt', 'r')
    s_bender_job_template = f_bender_job_template.read()
    f_bender_job_template.close()
    s_bender_job = s_bender_job_template.format(Nevents=Nevt_perjob, nfout=nTupleFileOut)
    f_bender_job = open(path_subjob_out+'Bender-Job_parallel.py', 'w')
    f_bender_job.write(s_bender_job)
    f_bender_job.close()
    
    f_run = open(path_subjob_out+'fbatch.sh', 'w')
    f_run.write('#!/bin/bash \n')
    f_run.write('. $VO_LHCB_SW_DIR/lib/LbLogin.sh\n')
#    f_run.write('LbLogin -c x86_64-centos7-gcc7-opt \n')
    f_run.write('cd %s \n'%(path_subjob_out))
#    f_run.write('%s/run gaudirun.py %s/Gauss-Job_parallel.py \n'%(path_gauss_run, path_subjob_out))
    f_run.write('lb-run -c best Gauss/v51r1 gaudirun.py %s/Gauss-Job_parallel.py \n'%(path_subjob_out))
    bender_command = 'lb-run bender/latest bender -b -d 2012 {path}/Gauss-{eventType}-{Nevents}ev-{date}.sim {path}/Bender-Job_parallel.py \n'.format(path=path_subjob_out, eventType=eventType, Nevents=Nevt_perjob, date=date)
    f_run.write(bender_command)

    f_run.close()
    os.system('chmod +x %s/fbatch.sh'%(path_subjob_out))

    os.mkdir(path_subjob_out+'out')
    os.mkdir(path_subjob_out+'err')
    #os.mkdir(path_subjob_conf+'log')

    '''
    f_sub = open(path_subjob_conf+'condor_config.sub', 'w')
    f_sub.write('executable            = %s/run.sh   \n'%(path_subjob_conf))
    f_sub.write('output                = %s/out/%i.$(ClusterId).$(ProcId).out   \n'%(path_subjob_conf,i))
    f_sub.write('error                 = %s/err/%i.$(ClusterId).$(ProcId).err   \n'%(path_subjob_conf,i))
    f_sub.write('log                   = %s/log/%i.$(ClusterId).log   \n'%(path_subjob_conf,i))
    f_sub.write('requirements          = ( (OpSysAndVer =?= "CentOS7") && (CERNEnvironment =?= "qa") )   \n')
    f_sub.write('+JobFlavour           = "espresso"   \n')
    f_sub.write('Priority              = high   \n')
    f_sub.write('Universe              = vanilla   \n')
    f_sub.write('should_transfer_files = IF_NEEDED   \n')
    f_sub.write('queue   \n')
    f_sub.close()
    '''
    
    command  = 'qsub '
    command += path_subjob_out+'fbatch.sh'
    command += ' -N fullSim_'+str(i)
    command += ' -o '+path_subjob_out+'out/out.txt'
    command += ' -e '+path_subjob_out+'err/err.txt'
    
    command = path_subjob_out+'fbatch.sh'
    
    os.system(command)

#    os.system('condor_submit %s/condor_config.sub'%(path_subjob_conf))
    
