from Gauss.Configuration import GenInit
from Gaudi.Configuration import *
eventType = '30000000'
importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-mu100-2012-nu2.5.py")

importOptions('$DECFILESOPTS/%s.py'%eventType)
#get Pgun options if necessary, otherwise load pythia8
pGun= (int(eventType)>50000000)

if True==pGun:    
    importOptions("$LBPGUNSROOT/options/PGuns.py")
    from Configurables import ToolSvc
    from Configurables import EvtGenDecay
    from Configurables import ParticleGun

    #if the dec file already has a particle gun configuration, pass it here, else, configure a flat momentum spectrum
    if hasattr(ParticleGun(),'SignalPdgCode'):
        print 'has attribute!'
        #no configuration necessary
        pass
    elif hasattr(ParticleGun(),'MomentumRange'):
        if hasattr(ParticleGun().MomentumRange,"PdgCodes"):
            print 'got PDGCodes. Should be Configured'
            pass
        else:
            print 'problem with configuration!'
            import sys
            sys.exit()
    else:
        print 'using flat momentum spectrum!'
        from Configurables import MomentumRange
        ParticleGun().addTool( MomentumRange )
        from GaudiKernel import SystemOfUnits
        ParticleGun().MomentumRange.MomentumMin = 1.0*SystemOfUnits.GeV
        from GaudiKernel import SystemOfUnits
        ParticleGun().MomentumRange.MomentumMax = 100.*SystemOfUnits.GeV
        ParticleGun().EventType = eventType
                
        ParticleGun().ParticleGunTool = "MomentumRange"
        ParticleGun().NumberOfParticlesTool = "FlatNParticles"
        #figure this out
        from Configurables import Generation
        pid_list = []
        if hasattr(Generation(),'SignalRepeatedHadronization'):
            pid_list = Generation().SignalRepeatedHadronization.SignalPIDList
        elif hasattr(Generation(),"SignalPlain"):
            pid_list = Generation().SignalPlain.SignalPIDList
        else:
            print 'major configuration problem, please fix!'
            import sys
            sys.exit()
        print 'got signal PID list',pid_list
        ParticleGun().MomentumRange.PdgCodes = pid_list
        ParticleGun().SignalPdgCode = abs(pid_list[0])
        ParticleGun().DecayTool="EvtGenDecay"
    
else:
    importOptions('$DECFILESOPTS/%s.py'%eventType)
    importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
    #fix pythia 8 color reconnection problem
    importOptions("$APPCONFIGOPTS/Gauss/TuningPythia8_Sim09.py")

#importOptions("$GAUSSOPTS/GenStandAlone.py")
importOptions('$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py')
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = 1

from Configurables import LHCbApp
LHCbApp().DDDBtag = 'dddb-20170721-3'
LHCbApp().CondDBtag = 'sim-20170721-2-vc-mu100'
LHCbApp().EvtMax = 10






