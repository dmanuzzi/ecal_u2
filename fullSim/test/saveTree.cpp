#include <iostream>

// from Gaudi
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/ToolFactory.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/GaudiTupleTool.h"
#include "GaudiAlg/Tuples.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiAlg/GaudiTupleAlg.h"
// Event.
#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// local
#include "TLorentzVector.h"
#include "TTree.h"
#include "TString.h"
#include "TFile.h"
int saveTree(){
  TString m_GeneratedParticles = "/Event/MC/Particles";
  TString ntout = "DecayTree";
  TString nfout = "testGenTree.root";
  LHCb::MCHeader* evt = get<LHCb::MCHeader>( LHCb::MCHeaderLocation::Default,IgnoreRootInTES);
  LHCb::MCParticles *m_genParticleContainer = get<LHCb::MCParticles>(m_GeneratedParticles);
  TTree tout(ntout, ntout);
  Double_t x(0.), y(0.), z(0.), px(0.), py(0.), pz(0.);
  Int_t PID(0);
  tout.Branch("x", &x);
  tout.Branch("y", &y);
  tout.Branch("z", &z);
  tout.Branch("px", &px);
  tout.Branch("py", &py);
  tout.Branch("pz", &pz);
  tout.Branch("PID", &PID);


  LHCb::MCParticles::const_iterator i;
  for( i=m_genParticleContainer->begin(); i != m_genParticleContainer->end(); i++ ){
    LHCb::MCParticle *P = (*i);
    const LHCb::MCVertex &Vtx = P->originVertex();
    const LHCb::ParticleID &ID = P->particleID();
    x = Vtx.position().X();
    y = Vtx.position().Y();
    z = Vtx.position().Z();
    //Double_t eta = P->pseudoRapidity();
    //Double_t phi = P->momentum().Phi();
    //Double_t theta = P->momentum().Theta();
    px = P->momentum().Px();
    py = P->momentum().Py();
    pz = P->momentum().Pz();
    //Double_t p = P->momentum().P();
    PID = ID.pid();
    tout.Fill();
  }
  TFile fout(nfout,"RECREATE");
  fout.WriteTObject(&tout, tout.GetName(), "koverwrite");
  fout.Close();
  return 1;
}
