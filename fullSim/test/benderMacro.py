print '****************************'
print '****************************'
print '*  Getting Info from DST   *'
print '****************************'
print '****************************'
ls()
from ROOT import TTree, TFile
from array import array


Nevents = int(1) 
nfout = str('stocazzo.root')
ntout = 'DecayTree'


fout = TFile(nfout, 'RECREATE')
PID = array( 'i', [0] )
key = array( 'i', [0] ) 
mother_key = array( 'i', [0] )
x_orig = array( 'f', [ 0.] )
y_orig = array( 'f', [ 0.] )
z_orig = array( 'f', [ 0.] )
t_orig = array( 'f', [ 0.] )
x_end = array( 'f', [ 0.] )
y_end = array( 'f', [ 0.] )
z_end = array( 'f', [ 0.] )
t_end = array( 'f', [ 0.] )
px = array( 'f', [ 0.] )
py = array( 'f', [ 0.] )
pz = array( 'f', [ 0.] )
E  = array( 'f', [ 0.] )
origVtype = array( 'i', [0] )
endVtype = array( 'i', [0] )

tout = TTree(ntout, ntout)
tout.Branch('PID', PID, 'PID/I');
tout.Branch('KEY', key, 'key/I');
tout.Branch('MC_MOTHER_KEY', mother_key, 'MC_MOTHER_KEY/I');
tout.Branch('x_orig', x_orig, 'x_orig/F');
tout.Branch('y_orig', y_orig, 'y_orig/F');
tout.Branch('z_orig', z_orig, 'z_orig/F');
tout.Branch('t_orig', t_orig, 't_orig/F');
tout.Branch('origVtype', origVtype, 'origVtype/I')
tout.Branch('x_end', x_end, 'x_end/F');
tout.Branch('y_end', y_end, 'y_end/F');
tout.Branch('z_end', z_end, 'z_end/F');
tout.Branch('t_end', t_end, 't_end/F')
tout.Branch('endVtype', endVtype, 'endVtype/I')
tout.Branch('px', px, 'px/F');
tout.Branch('py', py, 'py/F');
tout.Branch('pz', pz, 'pz/F');
tout.Branch('E', E, 'E/F');

for i_event in range(0,Nevents):
    particles = get('/Event/MC/Particles')
    for part in particles:
        PID[0] = part.particleID().pid()
        key[0] = part.key()
        if part.mother() != None:
            mother_key[0] = part.mother().key()
        else:
            mother_key[0] = -1
        #if not (PID in [11, -11, 22]): continue
        if part.originVertex() != None:
            x_orig[0] = part.originVertex().position().X()
            y_orig[0] = part.originVertex().position().Y()
            z_orig[0] = part.originVertex().position().Z()
            t_orig[0] = part.originVertex().time()
            origVtype[0] = part.originVertex().type()
        else:
            x_orig[0] = -999E9
            y_orig[0] = -999E9
            z_orig[0] = -999E9
            t_orig[0] = -999E9
            origVtype[0] = 0
        
        if (part.endVertices().size() != 0): 
            if part.endVertices().back().data() != None:
                x_end[0] = part.endVertices().back().data().position().X()
                y_end[0] = part.endVertices().back().data().position().Y()
                z_end[0] = part.endVertices().back().data().position().Z()
                t_end[0] = part.endVertices().back().data().time()
                endVtype[0] = part.endVertices().type()
        else: 
            x_end[0] = -999E9
            y_end[0] = -999E9
            z_end[0] = -999E9
            t_end[0] = -999E9
            endVtype[0] = 0
        
        px[0] = part.momentum().Px()
        py[0] = part.momentum().Py()
        pz[0] = part.momentum().Pz()
        E[0] = part.momentum().E()
        tout.Fill()
    run(1)

tout.Print()
fout.Write()
fout.Print()
fout.Close()
