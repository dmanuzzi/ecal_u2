 +---------------------------------------------------------------------------------------------------+
 | Location                                   |   Total  |       Mean      |   rms   |  min |   max  |
 +---------------------------------------------------------------------------------------------------+
 |  Gen/BeamParameters                        |        1 |       1         |         |      |        |
 |  Gen/Collisions                            |        2 |       2         |         |      |        |
 |  Gen/Header                                |        1 |       1         |         |      |        |
 |  Link/MC/Particles2MCRichTracks            |        1 |       1         |         |      |        |
 |  Link/MC/Rich/Hits2MCRichOpticalPhotons    |        1 |       1         |         |      |        |
 |  MC/Header                                 |        1 |       1         |         |      |        |
 |  pSim/Ecal/Hits                            |     1743 |    1743         |         |      |        |
 |  pSim/Hcal/Hits                            |      272 |     272         |         |      |        |
 |  pSim/IT/Hits                              |      187 |     187         |         |      |        |
 |  pSim/MCParticles                          |     1365 |    1365         |         |      |        |
 |  pSim/MCVertices                           |     1479 |    1479         |         |      |        |
 |  pSim/Muon/Hits                            |      204 |     204         |         |      |        |
 |  pSim/OT/Hits                              |      643 |     643         |         |      |        |
 |  pSim/Prs/Hits                             |     1401 |    1401         |         |      |        |
 |  pSim/PuVeto/Hits                          |       32 |      32         |         |      |        |
 |  pSim/Rich/Hits                            |        1 |       1         |         |      |        |
 |  pSim/Rich/OpticalPhotons                  |        1 |       1         |         |      |        |
 |  pSim/Rich/Segments                        |        1 |       1         |         |      |        |
 |  pSim/Rich/Tracks                          |        1 |       1         |         |      |        |
 |  pSim/Spd/Hits                             |      876 |     876         |         |      |        |
 |  pSim/TT/Hits                              |      272 |     272         |         |      |        |
 |  pSim/Velo/Hits                            |      574 |     574         |         |      |        |
 | *MC/Particles                              |     1365 |    1365         |         |      |        |
 | *MC/Rich/Hits                              |      804 |     804         |         |      |        |
 | *MC/Rich/OpticalPhotons                    |      804 |     804         |         |      |        |
 | *MC/Rich/Segments                          |       90 |      90         |         |      |        |
 | *MC/Rich/Tracks                            |       84 |      84         |         |      |        |
 | *MC/Vertices                               |     1479 |    1479         |         |      |        |
 +---------------------------------------------------------------------------------------------------+
   Analysed 0 events
