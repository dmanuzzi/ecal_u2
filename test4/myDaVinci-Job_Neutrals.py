##################################################
# simple pi0 particle maker using Delphes as input
# Adam Davis, last update 23/4/18
##################################################

#setup environment
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from PhysSelPython.Wrappers import Selection, SelectionSequence
from Configurables import DaVinci, DataOnDemandSvc,DecayTreeTuple,FilterDesktop, CombineParticles,PhotonMaker, PhotonMakerAlg
from Configurables import ResolvedPi0Maker

from CommonParticles.Utils import *
#get photons protoparticles, make them into particles.
#PhotonMaker takes directly from LHCb::ProtoParticleLocation::Neutrals
algorithm =  ResolvedPi0Maker ( 'StdLoosePi02gg',
                                DecayDescriptor = 'Pi0' )

# configure desktop&particle maker: 
algorithm.addTool(PhotonMaker , name = 'PhotonMaker' )
photon = algorithm.PhotonMaker
photon.ConvertedPhotons   = False
photon.UnconvertedPhotons = True
photon.PrsThreshold = -999.
photon.MaxPrsEnergy = 9.e9
photon.ConfidenceLevelBase   = []
photon.ConfidenceLevelSwitch = ['IsPhoton']
photon.PtCut              = 0. * MeV
#algorithm.OutputLevel=DEBUG
algorithm.InputPrimaryVertices = 'None'
## configure Data-On-Demand service 
locations = updateDoD ( algorithm )
#now filter on the mass a bit
from Configurables import FilterDesktop
mypi0Sel = Selection("mypi0Sel",
                     Algorithm = algorithm,
                     InputDataSetter= None)
mypi0SelSeq = SelectionSequence("mypi0SelSeq",TopSelection = mypi0Sel)

#input pi0s are set, make a decay tree tuple

tuple = DecayTreeTuple("pi0tree")
#tuple.OutputLevel=DEBUG
tuple.Inputs = [mypi0Sel.outputLocation()]
tuple.Decay = '[pi0 -> ^gamma ^gamma]CC'
tuple.ToolList+= ["TupleToolKinematic",
                  "TupleToolGeometry",
                  "TupleToolPrimaries",
                  "TupleToolEventInfo",
                  "TupleToolTrackInfo",
                  'TupleToolMCTruth'
                  ]
from Configurables import TupleToolMCTruth
tuple.ToolList+=['TupleToolMCTruth/ttmct']
tuple.addTool(TupleToolMCTruth('ttmct'))
tuple.ttmct.ToolList = [ "MCTupleToolHierarchy","MCTupleToolKinematic"]
tuple.ttmct.Verbose = True

from Configurables import DaVinci
DaVinci().PrintFreq = 50
DaVinci().TupleFile = "pi0_DV_Tuples_from_DelphesAlgs.root"

DaVinci().UserAlgorithms=[algorithm,
                          #fltr,
                          mypi0SelSeq]
DaVinci().appendToMainSequence([
    #fltr,
    mypi0SelSeq.sequence(),tuple])
DaVinci().InputType = 'DST'
DaVinci().Lumi = False
DaVinci().Simulation = True
DaVinci().DataType = "2012"
DaVinci().DDDBtag   = "dddb-20170721-3"
DaVinci().CondDBtag = "sim-20170721-2-vc-md100"
from GaudiConf import IOHelper
IOHelper().inputFiles(['Gauss-200ev-20190327.gen'],clear=True)
