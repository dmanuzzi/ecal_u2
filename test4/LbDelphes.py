## 
##  Example on how to run only the generator phase
##  It can be passed as additional argument to gaudirun.py directly
##   > gaudirun.py $APPCONFIGOPTS/Gauss/MC09-b5TeV-md100.py \
##                 $APPCONFIGOPTS/Conditions/MC09-20090602-vc-md100.py \
##                 $DECFILESROOT/options/EVENTTYPE.opts \
##                 $LBPYTHIAROOT/options/Pythia.opts \
##                 $GAUSSOPTS/GenStandAlone.py \
##                 $GAUSSOPTS/Gauss-Job.py
##  or you can set the property in your Gauss-Job.py
##  Port to python of GenStandAlone.opts
## 

from Configurables import Gauss
from Gaudi.Configuration import * 
import os
Gauss().Phases = ["Generator"]

def delphesForGauss():
    from Configurables import (GaudiSequencer, SimInit, DelphesAlg,
                               ApplicationMgr, DelphesHist, DelphesTuple,
                               DelphesProto, DelphesCaloProto, PGPrimaryVertex, BooleInit)

    #PGPrimaryVertex().OutputLevel=DEBUG
    PGPrimaryVertex().OutputVerticesName = '/Event/Rec/Vertex/Primary'
    PGPrimaryVertex().InputVerticesName = '/Event/MCFast/MCVertices'
    #DelphesAlg().LHCbDelphesCardLocation = '$LBDELPHESROOT/options/cards/delphes_card_LHCb.tcl'
#    DelphesAlg().LHCbDelphesCardLocation = '$LBDELPHESROOT/options/cards/delphes_card_LHCb_EndVelo_Rich2_withEcal.tcl'
    DelphesAlg().LHCbDelphesCardLocation = '$LBDELPHESROOT/options/cards/delphes_card_LHCb_DoubleStep_RAcceptance.tcl'
#    DelphesAlg().DelphesTrackLocation = 'FinalTrackMerger/tracks'
    DelphesAlg().DelphesTrackLocation = 'TrackMergerEndVelo/tracks'

#    DelphesAlg().OutputLevel=DEBUG
    DelphesCaloProto().LHCbDelphesCardLocation = '$LBDELPHESROOT/options/cards/delphes_card_LHCb_EndVelo_Rich2_withEcal.tcl'
    DelphesCaloProto().OutputLevel = DEBUG
    DelphesProto().TrackLUT = "$LBDELPHESROOT/LookUpTables/lutTrack.dat"
    DelphesProto().TrackCovarianceLUT = "$LBDELPHESROOT/LookUpTables/lutCovarianceProf.dat"
    #DelphesProto().OutputLevel=DEBUG
    #DelphesTuple().OutputLevel=DEBUG
    delphesSeq = GaudiSequencer("DelphesSeq")
    delphesSeq.Members = [ SimInit("InitDelphes") ]    
    delphesSeq.Members += [ DelphesAlg("DelphesAlg")]
    delphesSeq.Members += [ DelphesHist("DelphesHist") ]
    delphesSeq.Members += [ DelphesProto("DelphesProto") ]
    delphesSeq.Members += [ DelphesCaloProto("DelphesCaloProto") ]
    delphesSeq.Members += [ DelphesTuple("DelphesTuple") ]
    delphesSeq.Members += [ BooleInit("BooleInit", ModifyOdin = True) ]
    delphesSeq.Members += [ PGPrimaryVertex("PGPrimaryVertex")]
    delphesSeq.Members += [ GaudiSequencer("DelphesMonitor") ]

    ApplicationMgr().TopAlg += [ delphesSeq ]

appendPostConfigAction( delphesForGauss )

