from Gaudi.Configuration import *
from Configurables import Gauss, ApplicationMgr, UpdateManagerSvc
from Gauss.Configuration import *    

#importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-mu100-2012-nu2.5.py")
importOptions('$APPCONFIGOPTS/Gauss/Beam6500GeV-md100-2016-nu1.6.py')
importOptions('$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py')
importOptions('$APPCONFIGOPTS/Gauss/DataType-2016.py')

from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20170721-3"
LHCbApp().CondDBtag = "sim-20170721-2-vc-md100"

#eventType  = '56200001' #pi0 particle gun
eventType  = '11102421' #B0 -> KK pi0 
#importOptions("$LBPGUNSROOT/options/PGuns.py")
importOptions('./%s.py'%eventType)
importOptions('$LBPYTHIA8ROOT/options/Pythia8.py')
#importOptions('$GAUSSOPTS/GenStandAlone.py')
'''
from Configurables import ParticleGun
from Configurables import MomentumRange
ParticleGun().addTool( MomentumRange )
from GaudiKernel import SystemOfUnits
ParticleGun().MomentumRange.MomentumMin =  1.0*SystemOfUnits.GeV
ParticleGun().MomentumRange.MomentumMax = 50.0*SystemOfUnits.GeV
ParticleGun().EventType = 56200001
ParticleGun().ParticleGunTool = "MomentumRange"
ParticleGun().NumberOfParticlesTool = "FlatNParticles"
ParticleGun().MomentumRange.PdgCodes = [ 111 ]
from Configurables import ToolSvc
from Configurables import EvtGenDecay
ToolSvc().addTool( EvtGenDecay, name='EvtGenDecay' )
ToolSvc().EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/pi0,fixE=CaloAcc.dec"
ParticleGun().DecayTool = 'EvtGenDecay'
importOptions( "$DECFILESROOT/options/CaloAcceptance.py" )
'''
importOptions("./LbDelphes.py")

#--Number of events
nEvts = 3
LHCbApp().EvtMax = nEvts

#--Generator phase, set random numbers
gaussGen = GenInit("GaussGen")
gaussGen.FirstEventNumber = 1
gaussGen.RunNumber        = 1082

#--Set name of output files for given job and read in options
idFile = 'Gauss_'+str(eventType)
HistogramPersistencySvc().OutputFile = idFile+'_histos.root'
#--- Save ntuple with hadronic cross section information
ApplicationMgr().ExtSvc += [ "NTupleSvc" ]
NTupleSvc().Output = ["FILE1 DATAFILE='GaussTuple_{idFile}.root' TYP='ROOT' OPT='NEW'".format(idFile=eventType)]
       
FileCatalog().Catalogs = [ "xmlcatalog_file:Gauss_NewCatalog_0.xml" ]

def writeHits():    
    OutputStream("GaussTape").ItemList.append("/Event/MC/Vertices#1")
    OutputStream("GaussTape").ItemList.append("/Event/MC/Particles#1")
    OutputStream("GaussTape").ItemList.append("/Event/MCFast/MCParticles#1")
    OutputStream("GaussTape").ItemList.append("/Event/MCFast/MCVertices#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/ProtoP/Charged#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/ProtoP/Neutrals#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Track/Best#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Rich/PIDs#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Calo/Photons#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Calo/EcalClusters#1")
    OutputStream("GaussTape").ItemList.append("/Event/MC/DigiHeader#1")
    OutputStream("GaussTape").ItemList.append("/Event/DAQ/RawEvent#1")
    OutputStream("GaussTape").ItemList.append("/Event/DAQ/ODIN#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Vertex/Primary#1")
    print(OutputStream("GaussTape"))
#appendPostConfigAction(writeHits)

