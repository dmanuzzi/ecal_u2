#ifndef TOOL_H
#define TOOL_H
namespace paths{
  TString path_EOS = "root://eoslhcb.cern.ch//eos/lhcb/user/d/dmanuzzi/ECAL/";
  TString path_EOS_minBias_test0 = path_EOS+"20190606/minBias_test0/";
  TString path_EOS_minBias = path_EOS+"minBias_20190611/";
  TString path_EOS_minBias_upgrade0 = path_EOS_minBias+"evtType_30000000_Beam4000GeV_mu100_2012_nu2.5/";
  TString path_EOS_minBias_upgrade1 = path_EOS_minBias+"evtType_30000000_Beam7000GeV_md100_nu7.6/";
  TString path_EOS_gamma = path_EOS+"pGun_photons_20190607/";
  TString pGun_photons_evtType[10] = {"evtType_56000001", "evtType_56000010", "evtType_56000034", 
				      "evtType_56000100", "evtType_56000005", "evtType_56000017", 
				      "evtType_56000050", "evtType_56000168", 
				      "evtType_56000400", "evtType_56001200"};
};


#endif
